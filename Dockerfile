#Use the centos base image
FROM centos:latest
#set the maintainer
MAINTAINER Nic J
#Install http server
RUN yum -y install httpd
#copy files into the image
COPY --chown=apache index.html /var/www/html/
COPY --chown=apache laser.jpg /var/www/html/
#expose port 80
EXPOSE 80
#start httpd
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]